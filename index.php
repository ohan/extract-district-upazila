<?php
$total_district = 5;
$result_set = array();
for($i=1; $i <= $total_district ; $i++){
	$result_set[] = getUpazila($i);
}
//dd($result_set);
?>

<table border="1">
	<thead>
		<tr>
			<th>sl.</th>
			<th>upazila</th>
			<th>district</th>
		</tr>
	</thead>

<?php
$sl = 0;
foreach ($result_set as $value) {
	foreach ($value as $district => $upazilas) {
		foreach ($upazilas as $upazila) {
			echo '<tr><td>'.++$sl.'</td><td>'.$upazila.'</td><td>'.$district.'</td></tr>';
		}
	}
}
//d($result);
?>
</table>
<?php
function dd($var){
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
	exit;
}
function getPage($districtID) {
    $url = "http://www.lged.gov.bd/DistrictArea2.aspx?Area=Upazilla&DistrictID=".$districtID;
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 4);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    $page = curl_exec($ch);
    return $page;
}

function getUpazila($district_id){
	$page = getPage($district_id);
	preg_match_all('/UpazillaName.*>(.*)</', $page, $upazilas);
	preg_match('/DistrictName.*>(.*)<\/span>/', $page, $district);
	$district_name = $district[1];
	$upazila[$district_name] = $upazilas[1];
	return $upazila;
}
?>